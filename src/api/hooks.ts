import React, { useContext } from "react";
import { ApiCommandsService } from "./api-commands.service";
import { ApiQueriesService } from "./api-queries.service";

export const ApiCommandsContext = React.createContext(new ApiCommandsService());
export const ApiQueriesContext = React.createContext(new ApiQueriesService());

export const useApiCommands = () => {
  return useContext(ApiCommandsContext);
};

export const useApiQueries = () => {
  return useContext(ApiQueriesContext);
};
