import { axiosFactory } from "./axios";
import { QueryURIs } from "./uris";

export class ApiQueriesService {
  private _axios = axiosFactory();

  keys = {
    textSearchCustomers: "textSearchCustomers",
  };

  textSearchCustomers(
    _params: {
      q?: string;
      page?: number;
      pageSize?: number;
    } = {}
  ) {
    const params: {
      s: string;
      page: number;
      pageSize: number;
    } = Object.assign({ s: "", page: 1, pageSize: 10 }, _params);

    return this._axios
      .get<{
        page: number;
        pageSize: number;
        customers: {
          id: number;
          name: string;
          phoneNumber: string;
          email?: string;
        }[];
      }>(QueryURIs.customers.textSearch, { params })
      .then((res) => res.data);
  }
}
