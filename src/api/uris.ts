export const QueryURIs = {
  customers: {
    textSearch: "/customers/search",
  },
};

export const CommandURIs = {
  customers: {
    create: "/customers",
    delete: "/customers",
  },
};
