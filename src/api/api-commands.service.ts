import { axiosFactory } from "./axios";
import { CommandURIs } from "./uris";

export class ApiCommandsService {
  private _axios = axiosFactory();

  keys = {
    createCustomer: "createCustomer",
  };

  createCustomer(_params: {
    name: string;
    phoneNumber: string;
    email?: string;
  }) {
    return this._axios
      .post(CommandURIs.customers.create, _params)
      .then((res) => res.data);
  }

  deleteCustomer(_params: { phoneNumber: string }) {
    return this._axios
      .delete(CommandURIs.customers.delete, { data: _params })
      .then((res) => res.data);
  }
}
