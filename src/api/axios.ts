import axios from "axios";

export const axiosFactory = () =>
  axios.create({
    baseURL: process.env.REACT_APP_API_URL,
  });
