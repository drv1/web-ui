import React from "react";
import { Center, Container } from "@chakra-ui/react";
import { AddCustomerForm } from "../components/forms/AddCustomerForm";

export const AddCustomerPage = () => {
  return (
    <Center>
      <Container>
        <AddCustomerForm />
      </Container>
    </Center>
  );
};
