import React from "react";
import { SimpleGrid, Icon, Flex, Box } from "@chakra-ui/react";
import { LinkButton } from "../components/LinkButton";
import { BsPeopleFill } from "react-icons/bs";
import { IoMdPersonAdd } from "react-icons/io";

export const HomePage = () => {
  return (
    <Flex w="100%" height="100%" direction="column">
      <Box flex="1"></Box>
      <SimpleGrid w="100%" minChildWidth="64px" spacing="4" p="4">
        <LinkButton
          href="/customers"
          icon={<Icon as={BsPeopleFill} fontSize="xl" />}
          label="Buscar clientes"
        />
        <LinkButton
          href="/customers/add"
          icon={<Icon as={IoMdPersonAdd} fontSize="xl" />}
          label="Adicionar cliente"
        />
      </SimpleGrid>
    </Flex>
  );
};
