import React from "react";
import {
  Center,
  Flex,
  IconButton,
  Input,
  InputGroup,
  InputRightElement,
} from "@chakra-ui/react";
import { CustomerCardGridApiHoc } from "../components/api-hoc/CustomerCardGridApiHoc";
import { FaSearch } from "react-icons/fa";

export const CustomersPage = () => {
  const [term, setTerm] = React.useState("");
  const ref = React.createRef<HTMLInputElement>();
  const apply = () => {
    if (ref.current && ref.current.value !== term) setTerm(ref.current.value);
  };

  return (
    <Center>
      <Flex w="100%" direction="column" px="2">
        <form
          onSubmit={(e) => {
            apply();
            e.preventDefault();
          }}
        >
          <InputGroup>
            <Input placeholder="Buscar" ref={ref} />
            <InputRightElement>
              <IconButton
                type="submit"
                aria-label="Buscar"
                onSubmit={apply}
                onClick={apply}
              >
                <FaSearch />
              </IconButton>
            </InputRightElement>
          </InputGroup>
        </form>
        <CustomerCardGridApiHoc q={term} />
      </Flex>
    </Center>
  );
};
