import React from "react";
import { SimpleGrid } from "@chakra-ui/react";
import { CustomerCard } from "../cards/CustomerCard";

export type CustomerCardGridProps = {
  data: { name: string; phoneNumber: string; id: number }[];
};
export const CustomerCardGrid = ({ data }: CustomerCardGridProps) => {
  return (
    <SimpleGrid
      minChildWidth="240px"
      maxW="960px"
      spacingX="16px"
      spacingY="8px"
      w="full"
    >
      {data.map((customer, i) => (
        <CustomerCard key={i} customerData={customer} />
      ))}
    </SimpleGrid>
  );
};
