import React from "react";
import { Box, Button, Flex, Heading, Icon } from "@chakra-ui/react";
import { FaArrowLeft } from "react-icons/fa";
import { useGoBack } from "../../routerUtils";
import { useLocation } from "wouter";

export const TopBar = () => {
  const goBack = useGoBack();
  const [loc] = useLocation();

  return (
    <Box
      w="100%"
      boxSizing="border-box"
      h="64px"
      position="fixed"
      top="0"
      p="4"
      boxShadow="md"
    >
      <Flex h="100%" alignItems="center">
        {loc !== "/" && (
          <Button
            aria-label="Voltar"
            p="2"
            h="8"
            w="8"
            mr="4"
            bg="none"
            onClick={goBack}
          >
            <Icon as={FaArrowLeft} />
          </Button>
        )}
        <Heading fontSize="xl">DRV - Admin</Heading>
      </Flex>
    </Box>
  );
};
