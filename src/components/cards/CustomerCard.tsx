import React from "react";
import { Box, Button, Flex, Icon, Text } from "@chakra-ui/react";
import { FaWhatsapp } from "react-icons/fa";

export type CustomerCardProps = {
  customerData: { name: string; phoneNumber: string; id: number };
};
export const CustomerCard = ({ customerData }: CustomerCardProps) => {
  const { name, phoneNumber } = customerData;

  return (
    <Box rounded="md" p="4" boxShadow="md">
      <Flex justify="space-between">
        <Flex direction="column" flex="1">
          <Text fontSize="lg">{name}</Text>
          <Text fontSize="sm" color="gray.400">
            {phoneNumber}
          </Text>
        </Flex>
        <Flex direction="column" w="min-content">
          <Button p="2" bg="none" aria-label="abrir whatsapp">
            <Icon as={FaWhatsapp} h="100%" w="100%" />
          </Button>
        </Flex>
      </Flex>
    </Box>
  );
};
