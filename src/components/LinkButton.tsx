import React from "react";
import {
  Button,
  Flex,
  Link as ChakraLink,
  SimpleGrid,
  Text,
} from "@chakra-ui/react";
import { Link } from "wouter";

export const LinkButton = (props: {
  href: string;
  label: string;
  icon?: React.ReactNode;
}) => (
  <ChakraLink as={Link} href={props.href}>
    <Flex
      as={Button}
      h="32"
      p="4"
      justifyContent="flex-start"
      alignItems="flex-end"
    >
      <SimpleGrid direction="column" columns={1} spacingY="8px">
        {props.icon}
        <Text>{props.label}</Text>
      </SimpleGrid>
    </Flex>
  </ChakraLink>
);
