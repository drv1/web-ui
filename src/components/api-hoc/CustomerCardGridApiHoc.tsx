import React from "react";
import { useQuery } from "react-query";
import { useApiQueries } from "../../api/hooks";
import { CustomerCardGrid } from "../grids/CustomerCardGrid";

export const CustomerCardGridApiHoc = (props: { q?: string }) => {
  const apiQueries = useApiQueries();
  const query = useQuery(
    [apiQueries.keys.textSearchCustomers, props.q || ""],
    () => apiQueries.textSearchCustomers({ q: props.q }),
    {
      initialData: { customers: [], page: 1, pageSize: 0 },
      keepPreviousData: true,
    }
  );

  return <CustomerCardGrid data={query.data?.customers || []} />;
};
