import React from "react";
import InputMask from "react-input-mask";
import {
  Field,
  FieldProps,
  Form,
  FormikErrors,
  FormikProps,
  withFormik,
} from "formik";
import validator from "validator";
import {
  Button,
  FormControl,
  FormErrorMessage,
  FormLabel,
  Input,
  SimpleGrid,
  useToast,
} from "@chakra-ui/react";
import { useApiCommands } from "../../api/hooks";
import { ApiCommandsService } from "../../api/api-commands.service";

interface FormValues {
  name: string;
  phoneNumber: string;
  email?: string;
}

const validators = {
  name: (s?: string) => {
    if (!s) return "O nome é obrigatório";
    if (s.length > 256) return "O nome deve ter até 256 caraceteres";
    if (s.length < 1) return "O nome deve ter mais de 1 caracter";
  },
  email: (s?: string) => {
    if (!s) return;
    if (!validator.isEmail(s)) return "Email inválido";
  },
  phoneNumber: (s?: string) => {
    if (!s) return "O telefone é obrigatório";
    if (!validator.isMobilePhone(s, "pt-BR", { strictMode: true }))
      return "Telefone inválido";
  },
};

const InnerForm = (props: FormikProps<FormValues>) => {
  return (
    <Form>
      <SimpleGrid spacingY="8px">
        <Field name="name" validate={validators.name}>
          {({
            field: { value: fieldValue, ...field },
            form,
          }: FieldProps<string, FormValues>) => (
            <FormControl isInvalid={!!(form.errors.name && form.touched.name)}>
              <FormLabel htmlFor="name">Nome</FormLabel>
              <Input
                value={fieldValue || ""}
                {...field}
                id="name"
                placeholder="Nome"
              />
              <FormErrorMessage>{form.errors.name}</FormErrorMessage>
            </FormControl>
          )}
        </Field>
        <Field name="phoneNumber" validate={validators.phoneNumber}>
          {({
            field: { value: fieldValue, ...field },
            form,
          }: FieldProps<string, FormValues>) => (
            <FormControl
              isInvalid={
                !!(form.errors.phoneNumber && form.touched.phoneNumber)
              }
            >
              <FormLabel htmlFor="phoneNumber">Telefone (WhatsApp)</FormLabel>
              <Input
                as={InputMask}
                value={fieldValue || ""}
                {...field}
                mask="+55 (99) 99999-9999"
                id="phoneNumber"
                placeholder="Telefone"
              />
              <FormErrorMessage>{form.errors.phoneNumber}</FormErrorMessage>
            </FormControl>
          )}
        </Field>
        <Field name="email" validate={validators.email}>
          {({
            field: { value: fieldValue, ...field },
            form,
          }: FieldProps<string, FormValues>) => (
            <FormControl
              isInvalid={!!(form.errors.email && form.touched.email)}
            >
              <FormLabel htmlFor="email">Email</FormLabel>
              <Input
                value={fieldValue || ""}
                {...field}
                type="email"
                id="email"
                placeholder="Email"
              />
              <FormErrorMessage>{form.errors.email}</FormErrorMessage>
            </FormControl>
          )}
        </Field>
      </SimpleGrid>
      <Button
        mt={4}
        colorScheme="teal"
        isLoading={props.isSubmitting}
        type="submit"
      >
        Adicionar
      </Button>
    </Form>
  );
};

export const AddCustomerFormWrapped = withFormik<
  { apiCommands: ApiCommandsService; toast: any },
  FormValues
>({
  validate: (values: FormValues) => {
    const errors: FormikErrors<FormValues> = {
      name: validators.name(values.name),
      email: validators.email(values.email),
      phoneNumber: validators.phoneNumber(values.phoneNumber),
    };
    if (Object.values(errors).some((x) => x)) return errors;
  },
  handleSubmit: ({ name, phoneNumber, email }, bag) => {
    return bag.props.apiCommands
      .createCustomer({ name, phoneNumber, email })
      .then(() =>
        bag.props.toast({
          title: "Cliente adicionado com sucesso",
          status: "success",
          duration: 5000,
          isClosable: true,
        })
      )
      .catch(() =>
        bag.props.toast({
          title: "Erro ao adicionar cliente",
          status: "error",
          duration: 5000,
          isClosable: true,
        })
      );
  },
})(InnerForm);

export const AddCustomerForm = () => {
  const apiCommands = useApiCommands();
  const toast = useToast();

  return <AddCustomerFormWrapped apiCommands={apiCommands} toast={toast} />;
};
