import { Box, Flex } from "@chakra-ui/react";
import React from "react";
import { TopBar } from "./components/floating/TopBar";

export const Layout: React.FC = ({ children }) => {
  return (
    <Flex pt="80px" minH="100vh" direction="column">
      <TopBar />
      <Box flex="1" height="calc(100vh - 80px)">
        {children}
      </Box>
    </Flex>
  );
};
