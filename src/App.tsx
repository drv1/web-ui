import { config } from "dotenv";
import * as React from "react";
import { ChakraProvider, extendTheme } from "@chakra-ui/react";
import { Route } from "wouter";
import { HomePage } from "./pages/HomePage";
import "./global.css";
import { AddCustomerPage } from "./pages/AddCustomerPage";
import { CustomersPage } from "./pages/CustomersPage";
import { QueryClient, QueryClientProvider } from "react-query";
import { Layout } from "./Layout";

config();

const chakraTheme = extendTheme({
  fonts: {
    heading: "Titillium Web",
    body: "Titillium Web",
  },
  config: { initialColorMode: "dark" },
});

export const App = () => {
  return (
    <ChakraProvider theme={chakraTheme}>
      <QueryClientProvider client={new QueryClient()}>
        <Layout>
          <Route path="/">
            <HomePage />
          </Route>
          <Route path="/customers/add">
            <AddCustomerPage />
          </Route>
          <Route path="/customers">
            <CustomersPage />
          </Route>
        </Layout>
      </QueryClientProvider>
    </ChakraProvider>
  );
};
