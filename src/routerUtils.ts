import { useLocation } from "wouter";

export const useGoBack = () => {
  const [loc, setLoc] = useLocation();
  const dest = loc.split("/").slice(0, -1).join("/");
  return () => {
    setLoc(dest || "/");
  };
};

// const StackHistoryContext = React.createContext<{
//   append: (s: string) => void;
//   pop: () => void;
//   get: () => string[];
// }>({
//   append: (s: string) => {},
//   pop: () => {},
//   get: () => [],
// });

// StackHistoryContext.displayName = "StackHistoryContext";

// export const StackHistoryProvider = StackHistoryContext.Provider;
// export const useStackHistory = () => React.useContext(StackHistoryContext);

// export const useProvideStackHistory = () => {
//   const [history, setHistory] = React.useState<string[]>([]);
//   const stackHistory = {
//     append: (s: string) => setHistory([...history, s]),
//     pop: () => {
//       setHistory(history.slice(-1));
//     },
//     get: () => history,
//   };

//   return stackHistory;
// };

// export const useStackNavigation = () => {
//   const stackHistory = useStackHistory();
//   const [_, setLoc] = useLocation();

//   return {
//     push: (uri: string) => {
//       stackHistory.append(uri);
//       setLoc(uri);
//     },
//     pop: () => {
//       const dest = stackHistory.get().slice(-1)[0];
//       stackHistory.pop();
//       setLoc(dest);
//     },
//   };
// };
